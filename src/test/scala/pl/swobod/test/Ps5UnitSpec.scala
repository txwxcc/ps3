package pl.swobod.test

import org.scalatest._
import pl.swobod.Ps5Task


class Ps5UnitSpec extends FlatSpec with Matchers
  with OptionValues with Inside with Inspectors {

  "Ps5" should "pass custom test" in {
    val input = "7 10 2\n1 2 0\n2 4 10\n4 7 1\n5 7 4\n3 4 2\n3 5 0\n5 2 2\n6 3 3\n7 6 3\n1 3 9\n"
    val output = "5\n1 2 5 3 4 7\n"

    assert(Ps5Task(input).run() == output)
  }

  "Ps5" should "pass #1 test" in {
    val input = "10 20 1\n5 8 42\n4 9 0\n7 9 93\n1 4 5\n7 10 45\n5 7 34\n9 10 37\n4 6 0\n2 6 82\n6 7 80\n6 9 8\n8 10 17\n3 6 96\n1 2 0\n5 9 33\n3 8 21\n5 10 26\n8 9 6\n6 10 25\n2 7 90\n"
    val output = "28\n1 4 9 8 10\n"

    assert(Ps5Task(input).run() == output)
  }

  "Ps5" should "pass #2 test" in {
    val input = "20 50 2\n10 13 99\n12 19 31\n16 17 7\n18 19 58\n13 20 99\n17 20 68\n3 4 52\n11 14 3\n3 7 95\n12 16 45\n4 10 76\n19 20 34\n9 19 0\n2 11 100\n4 11 40\n17 19 0\n11 19 53\n12 20 80\n13 19 97\n7 12 0\n2 10 17\n17 18 45\n4 9 17\n5 6 9\n5 13 52\n18 20 97\n11 15 89\n15 17 25\n1 10 88\n3 13 0\n8 11 8\n7 11 56\n8 15 16\n12 14 67\n7 10 16\n14 19 56\n7 13 32\n14 17 0\n7 15 74\n11 18 3\n9 14 5\n7 17 32\n11 17 81\n14 15 77\n10 15 97\n9 15 0\n3 11 85\n2 9 55\n10 11 82\n4 8 3\n"
    val output = "169\n1 10 7 12 19 20\n"

    assert(Ps5Task(input).run() == output)
  }

  "Ps5" should "pass #3 test" in {
    val input = "30 60 3\n4 11 17\n22 23 58\n24 28 74\n24 30 97\n19 21 47\n13 15 25\n6 7 39\n24 27 94\n8 12 93\n10 12 5\n5 12 13\n25 27 53\n16 18 44\n20 30 29\n24 29 30\n5 13 36\n1 7 0\n14 16 32\n6 10 0\n8 18 53\n6 9 65\n25 30 39\n1 2 39\n22 27 35\n10 13 77\n13 16 58\n29 30 46\n9 10 0\n27 30 88\n8 17 58\n17 25 24\n9 19 62\n1 11 48\n3 5 25\n26 27 72\n10 18 25\n28 29 0\n21 28 28\n9 16 85\n21 22 36\n16 26 83\n20 23 0\n12 13 0\n16 19 81\n20 24 56\n2 7 0\n24 25 23\n6 13 73\n26 29 60\n17 20 61\n17 18 0\n10 14 16\n27 28 99\n7 10 76\n25 29 0\n25 28 57\n21 25 59\n3 12 98\n22 30 59\n22 28 16\n"
    val output = "127\n1 7 6 10 18 17 25 30\n"

    assert(Ps5Task(input).run() == output)
  }
}