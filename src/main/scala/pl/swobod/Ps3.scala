package pl.swobod

import scala.collection.mutable.ListBuffer
import scala.io.StdIn

object Ps3 {
  def main(args: Array[String]): Unit = print(start())

  def start(input: Option[String] = None): String = {
    input.fold(parse())(parse).doYour() match {
      case Some(r) =>
        var res = r._1. toString + "\n"
        for (step <- r._3) {
          res = res + step + " "
        }
        res.substring(0, res.length - 1) + "\n"
      case None => "NIE\n"
    }
  }

  def parse(): Ps3Task = {
    val top = StdIn.readLine().split(" ")
    val task = new Ps3Task(top(0).toInt, top(1).toInt, top(2).toInt)

    var counter = 0
    for (ln <- io.Source.stdin.getLines) {
      val tmp = ln.split(" ")
      task.setEdge(counter, (tmp(0).toInt, tmp(1).toInt, tmp(2).toInt, tmp(3).toInt, counter + 1))

      counter += 1
      if (counter >= task.m) return task
    }
    task
  }

  def parse(input: String): Ps3Task = {
    val top = input.split("\n")(0).split(" ")
    val task = new Ps3Task(top(0).toInt, top(1).toInt, top(2).toInt)

    var counter = 0
    for (ln <- input.split("\n").tail.iterator) {
      val tmp = ln.split(" ")
      task.setEdge(counter, (tmp(0).toInt, tmp(1).toInt, tmp(2).toInt, tmp(3).toInt, counter + 1))
      counter += 1
    }
    task
  }

  class Ps3Task(val n: Int, val m: Int, val p: Int) {
    val edge = new Array[(Int, Int, Int, Int, Int)](m)
    val resultTab = Array.fill[ListBuffer[(Int, Int, List[Int])]](n)(new ListBuffer[(Int, Int, List[Int])])

    resultTab(0).+=((0, 0, List[Int]()))

    def setEdge(i: Int, c: (Int, Int, Int, Int, Int)): Unit = edge(i) = c

    def doYour(): Option[(Int, Int, List[Int])] = {
      for (i <- 0 to n - 2; e <- edge.filter(_._1 == i + 1)) {
        val endPoint = resultTab(e._2 - 1)

        for (startPoint <- resultTab(e._1 - 1)) {
          putIn(endPoint,
            (startPoint._1 + e._3,
              startPoint._2 + e._4,
              startPoint._3 ::: List[Int](e._5)))
        }
      }

      if (resultTab(n - 1).nonEmpty) {
        var finalResult = resultTab(n - 1).head

        for (result <- resultTab(n - 1)) {
          if (result._1 < finalResult._1)
            finalResult = result
        }

        Some(finalResult)
      } else None
    }

    private def putIn(container: ListBuffer[(Int, Int, List[Int])], element: (Int, Int, List[Int])): Unit = {
      if (element._2 <= p) {
        if (container.nonEmpty) {
          val allWorseThan = new ListBuffer[(Int, Int, List[Int])]

          for (caseTuple <- container) {
            if ((caseTuple._2 == element._2 && caseTuple._1 <= element._1)
              || (caseTuple._2 <= element._2 && caseTuple._1 == element._1)) {
              return
            }

            if ((caseTuple._2 == element._2 && caseTuple._1 >= element._1)
              || (caseTuple._2 >= element._2 && caseTuple._1 == element._1)) {
              allWorseThan.append(caseTuple)
            }
          }

          for (junk <- allWorseThan) {
            container.remove(container.indexOf(junk))
          }
        }

        container.append(element)
      }
    }
  }
}
