package pl.swobod

import scala.collection.mutable.ListBuffer
import scala.io.StdIn


object Ps5 {
  def main(args: Array[String]): Unit = print(Ps5Task() run())
}

object Ps5Task {
  def apply(): Ps5Task = {  // Parser. Wczytywanie ze standardowego wejścia parametrów n, m, k z pierwszej linii
    val params = StdIn readLine() split " "

    new Ps5Task(params(0).toInt, params(1).toInt, params(2).toInt)
  }

  def apply(input: String): Ps5Task = { // Parser. Wczytywanie z stringa parametrów n, m, k z pierwszej linii
    val tunnels = input.lines
    val params = tunnels next() split " "

    new Ps5Task(params(0).toInt, tunnels, params(2).toInt)
  }
}

class Ps5Task private(val numberOfCities: Int, val maxInstantTunnels: Int) {
  var bestPaths: Array[ListBuffer[Steps]] = Array.ofDim[ListBuffer[Steps]](numberOfCities)
  var traversingPaths: ListBuffer[Steps] = new ListBuffer[Steps]()

  //
  class Steps private(val steps: List[Tunnel], val totalTime: Int, val usedInstantTunnels: Int) {
    def this(tunnels: List[Tunnel]) = this(tunnels, tunnels.map(_.time).sum, tunnels.count(p => p.time == 0))
    def addNext(tunnel: Tunnel): Steps = new Steps(
      steps :+ tunnel, totalTime + tunnel.time,
      if (tunnel.time == 0) usedInstantTunnels + 1 else usedInstantTunnels)
  }
  case class Tunnel(destination: City, time: Int) // Miasto docelowe i czas

  class City (val id: Int, val tunnels: ListBuffer[Tunnel] = ListBuffer()) {  // Pudełeczko na Sasiady
    Cities.all(id - 1) = this
  }

  object Cities { // Może być jako statyczne dla City
    val all: Array[City] = Array.ofDim(numberOfCities)

    def apply(id: Int): City = all(id - 1) match {
      case c: City => c
      case null => new City(id)
    }
  }

  def this(n: Int, m: Int, k: Int) {  // Parser. Wczytywanie ze standardowego wejścia
    this(n, k)

    for (i <- 1 to m) {
      val word = StdIn.readLine split " "
      Cities(word(0).toInt).tunnels.append(Tunnel(Cities(word(1).toInt), word(2).toInt))
      Cities(word(1).toInt).tunnels.append(Tunnel(Cities(word(0).toInt), word(2).toInt))  // Redundantny tunel w przeciwnym kierunku
    }
  }

  def this(n: Int, tunnels: Iterator[String], k: Int) { // Parser. Wczytywanie z stringa (do testów)
    this(n, k)

    for (tunnel <- tunnels) {
      val word = tunnel split " "

      val (source, destination, time) = (
        Cities(word(0).toInt),
        Cities(word(1).toInt),
        word(2).toInt
      )
      source.tunnels append Tunnel(destination, time)
      destination.tunnels append Tunnel(source, time) // Redundantny tunel w przeciwnym kierunku
    }
  }

  def run(): String = {
    for (i <- bestPaths.indices) bestPaths(i) = new ListBuffer[Steps]()

    Cities(1).tunnels.foreach(t => store(new Steps(List(t))))

    while (traversingPaths.nonEmpty) {
      val paths = traversingPaths.clone()
      traversingPaths.clear()

      for (path <- paths)
        path.steps.last.destination.tunnels foreach {tunnel =>  //  OR: for (tunnel <- path.steps.last.destination.tunnels)

          if (!path.steps.exists(_.destination == tunnel.destination))
            store(path addNext tunnel)
        }
    }

    echo()
  }

  def store(steps: Steps): Unit = {
    val mnt = bestPaths(steps.steps.last.destination.id - 1)

    if (mnt.nonEmpty) {
      var del: Option[Int] = None

      for (i <- mnt.indices) {
        if ((mnt(i).totalTime <= steps.totalTime && mnt(i).totalTime <= steps.totalTime)
          && !(mnt(i).totalTime == steps.totalTime && mnt(i).totalTime == steps.totalTime))
          return
        if ((mnt(i).totalTime <= steps.totalTime && mnt(i).totalTime <= steps.totalTime)
          && !(mnt(i).totalTime == steps.totalTime && mnt(i).totalTime == steps.totalTime))
          del = Some(i)
      }

      del foreach mnt.drop
    }

    mnt append steps
    traversingPaths append steps
  }

  def echo(): String = {
    var fastest = bestPaths.last.head

    bestPaths.last.foreach(path =>
      if(path.totalTime < fastest.totalTime)
        fastest = path
    )

    fastest.totalTime.toString + "\n1 " +
      String.join(" ", fastest.steps.map(_.destination.id.toString):_*) + "\n"
  }
}